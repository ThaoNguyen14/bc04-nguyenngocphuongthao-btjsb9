function layThongTinTuForm() {
  const maSV = document.getElementById("txtMaSV").value;
  const tenSV = document.getElementById("txtTenSV").value;
  const emailSV = document.getElementById("txtEmail").value;
  const passSV = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  return new SinhVien(tenSV, maSV, emailSV, passSV, diemToan, diemLy, diemHoa);
}

// render array SV ra giao dien
function renderDSSV(svArr) {
  console.log("svArr: ", svArr);
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    //trContent thẻ tr trong mỗi lần lặp
    var trContent = `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.Email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <btn onclick="xoaSinhVien(${sv.ma})" class = "btn btn-danger">Xóa</btn>
    <btn onclick="suaSinhVien(${sv.ma})"class = "btn btn-warning">Sửa</btn>
    
    </td>

    </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
  // tbodySinhVien;
}

function timKiemViTri(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.ma == id) {
      return index;
    }
  }
  //ko tìm thấy
  return -1;
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.Email;
  document.getElementById("txtPass").value = sv.Matkhau;
  document.getElementById("txtDiemToan").value = sv.Toan;
  document.getElementById("txtDiemLy").value = sv.Ly;
  document.getElementById("txtDiemHoa").value = sv.Hoa;
}
