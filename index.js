// chức năng thêm SV
var dssv = [];
// lấy thông tin từ localStorage
var dssvJson = localStorage.getItem("DSSV");
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);

  // array khi convert thành json sẽ mất function, ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];

    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.Matkhau,
      sv.Email,
      sv.Toan,
      sv.Ly,
      sv.Hoa
    );
  }

  renderDSSV(dssv);
}

function themSV() {
  var newSV = layThongTinTuForm();
  //console.log("newSV: ", newSV);
  // kiem tra ma SV
  var isValid =
    validator.kiemTraRong(newSV.ma, "spanMaSV", "Mã SV không được để rỗng") &&
    validator.kiemTraDoDai(
      newSV.ma,
      "spanMaSV",
      "Mã SV phải gồm 4 ký tự",
      4,
      4
    );
  //kiem tra ten SV
  isValid =
    isValid &
    validator.kiemTraRong(newSV.ten, "spanTenSV", "Tên SV không được để rỗng");

  //kiem tra email SV
  isValid =
    isValid &
    validator.kiemTraRong(
      newSV.Email,
      "spanEmailSV",
      "Email SV không được để rỗng"
    );

  //kiem tra password ko duoc rong
  isValid =
    isValid &
    isValid &
    validator.kiemTraRong(
      newSV.Matkhau,
      "spanMatKhau",
      "Mật khẩu SV không được để rỗng"
    );

  //kiem tra toan, ly , hoa khong duoc rong va la so
  isValid =
    isValid &
    isValid &
    isValid &
    isValid &
    validator.kiemTraDiemSo(
      newSV.Toan,
      "spanToan",
      "Điểm Toán không được để trống và phải là số"
    );

  isValid =
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    validator.kiemTraRong(
      newSV.Ly,
      "spanLy",
      "Điểm Lý không được để trống và phải là số"
    );

  isValid =
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    validator.kiemTraRong(
      newSV.Hoa,
      "spanHoa",
      "Điểm Hóa không được để trống và phải là số"
    );

  // nếu hợp lệ mới thêm
  if (isValid) {
    dssv.push(newSV);

    // tạo json
    var dssvJson = JSON.stringify(dssv);
    // luu json vào localstorage
    localStorage.setItem("DSSV", dssvJson);

    renderDSSV(dssv);

    //console.log("dssv: ", dssv);
  }
}

function xoaSinhVien(id) {
  console.log("id: ", id);

  var index = timKiemViTri(id, dssv);
  // tìm thấy vị trí
  if (index != -1);
  dssv.splice(index, 1);
  renderDSSV(dssv);
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}

function capnhatSinhVien(id) {
  xoaSinhVien();
  document.getElementById("txtMaSV").setAttribute("disabled", true);

  themSV();
}

function resetSinhVien() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}

function searchSinhVien() {
  var tenSVCanTim = document.getElementById("txtSearch").value;
  var ketqua = [];
  if (tenSVCanTim) {
    ketqua = dssv.filter((sv) => sv.ten.includes(tenSVCanTim)); // => tra ve mang co phan tu hoac rong
  } else {
    ketqua = dssv;
  }
  if (ketqua.length) {
    renderDSSV(ketqua);
  } else {
    document.getElementById("tbodySinhVien").innerHTML = "No Result!";
  }

  console.log({ tenSVCanTim });
}
